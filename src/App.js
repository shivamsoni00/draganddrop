// import FileUpload from './Component/files'
import DragDown from './Component/Drag'
import {useCallback} from 'react'
import { useState } from "react";
import axios from 'axios'



function App() {
  const [images, setImages] = useState([]);

  console.log(images)
  
  const onDrop = useCallback(acceptedFiles => {
    acceptedFiles.map(file => {
      const reader = new FileReader();
      reader.onload = function(e) { 
        setImages(prevState => [
          ...prevState,
          {src: e.target.result }
        ]);
      };
      reader.readAsDataURL(file);
      return file;
    });
  }, []);


 const onFileUpload = () => {
    
  let payload = {
    image_list: images
  };
  console.log(payload)

  axios({
    url: 'http://localhost:5000/upload',
    method: 'post',
    data: payload
  })
  .then(function (response) {
      console.log(response);
  })
  .catch(function (error) {
      console.log(error);
  });
   // axios.post("http://localhost:5000/upload", formData);
  };

  return(
    <>
    <div>
      <DragDown onDrop={onDrop} accept={"image/*"}/>
    </div>

    <div>
      <button onClick={onFileUpload}>Upload!</button>
    </div>
    </>
  )
}

export default App




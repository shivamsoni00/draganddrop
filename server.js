const express = require("express");
const multer = require("multer");
const cors = require('cors')


const app = express();
app.use(express.json());

app.use(cors())



const upload = multer({ dest: "public/" });


app.post("/upload", upload.single("file"), uploadFiles);

function uploadFiles(req, res) {
    console.log(req.body);
    console.log(req.file);
    res.json({ message: "Successfully uploaded files" });
}


app.listen(5000, () => {
    console.log(`Server started...`);
});